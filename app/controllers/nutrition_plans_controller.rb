class NutritionPlansController < ApplicationController
  before_action :set_nutrition_plan, except: [:index, :new, :create]

  def index
    @nutrition_plans = NutritionPlan.all
  end

  def show
  end

  def new
    @nutrition_plan = NutritionPlan.new
  end

  def edit
  end

  def create
    @nutrition_plan = NutritionPlan.new(nutrition_plan_params)

    if @nutrition_plan.save
      redirect_to(
        @nutrition_plan,
        notice: 'Nutrition plan was successfully created.'
      )
    else
      render :new
    end
  end

  def update
    if @nutrition_plan.update(nutrition_plan_params)
      redirect_to(
        @nutrition_plan,
        notice: 'Nutrition plan was successfully updated.'
      )
    else
      render :edit
    end
  end

  def destroy
    @nutrition_plan.destroy

    redirect_to(
      nutrition_plans_url,
      notice: 'Nutrition plan was successfully destroyed.'
    )
  end

  def show_dishes
    @dishes = Dish.all
  end

  def add_dishes
    dishes_ids = params.require(:nutrition_plan)[:dish_ids].reject(&:empty?)
    @nutrition_plan.dishes = Dish.find(dishes_ids)

    redirect_to(
        @nutrition_plan,
        notice: 'Dishes were added successfully.'
      )
  end

  def remove_dish
    @nutrition_plan.dishes.delete(params.require(:dish))

    redirect_to(
      nutrition_plan_path(@nutrition_plan),
      notice: 'Dish was successfully removed.'
    )
  end

  private

  def set_nutrition_plan
    @nutrition_plan = NutritionPlan.find(params[:id])
  end

  def nutrition_plan_params
    params.require(:nutrition_plan).permit(:name)
  end
end
