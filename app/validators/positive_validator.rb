class PositiveValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return false if value.nil?
    record.errors.add attribute, 'can\'t be negative' if value < 0
  end
end
