class NutritionPlan < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  has_many :dishes_nutrition_plans
  has_many :dishes, through: :dishes_nutrition_plans

  def total_nutritional_value
    result = { calories: 0, protein: 0, carbohydrates: 0, fat: 0 }

    dishes.each do |dish|
      result[:calories] += dish.calories
      result[:protein] += dish.protein
      result[:carbohydrates] += dish.carbohydrates
      result[:fat] += dish.fat
    end

    result
  end
end
