class Dish < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  validates :calories, presence: true, positive: true
  validates :protein, presence: true, positive: true
  validates :carbohydrates, presence: true, positive: true
  validates :fat, presence: true, positive: true

  has_many :dishes_nutrition_plans
  has_many :nutrition_plans, through: :dishes_nutrition_plans
end
