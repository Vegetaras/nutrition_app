class DishesNutritionPlan < ActiveRecord::Base
  belongs_to :dish
  belongs_to :nutrition_plan
end
