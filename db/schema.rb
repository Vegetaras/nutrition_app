# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141020205109) do

  create_table "dishes", force: true do |t|
    t.string   "name"
    t.integer  "calories"
    t.integer  "protein"
    t.integer  "carbohydrates"
    t.integer  "fat"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dishes_nutrition_plans", force: true do |t|
    t.integer "dish_id"
    t.integer "nutrition_plan_id"
  end

  add_index "dishes_nutrition_plans", ["dish_id", "nutrition_plan_id"], name: "index_dishes_nutrition_plans_on_dish_id_and_nutrition_plan_id"

  create_table "nutrition_plans", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
