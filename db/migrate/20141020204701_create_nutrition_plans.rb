class CreateNutritionPlans < ActiveRecord::Migration
  def change
    create_table :nutrition_plans do |t|
      t.string :name

      t.timestamps
    end
  end
end
