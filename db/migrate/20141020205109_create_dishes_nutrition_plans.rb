class CreateDishesNutritionPlans < ActiveRecord::Migration
  def change
    create_table :dishes_nutrition_plans do |t|
      t.column :dish_id, :integer
      t.column :nutrition_plan_id, :integer
    end
    add_index :dishes_nutrition_plans, [:dish_id, :nutrition_plan_id]
  end
end
