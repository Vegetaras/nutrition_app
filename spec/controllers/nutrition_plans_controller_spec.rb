require 'rails_helper'

describe NutritionPlansController, type: :controller do

  describe 'GET #index' do
    it 'renders index template' do
      get :index
      expect(response).to render_template('index')
    end

    it 'assigns @nutrition_plans' do
      nutrition_plan = create(:nutrition_plan)
      get :index
      expect(assigns(:nutrition_plans)).to eq([nutrition_plan])
    end
  end

  describe 'GET #show' do
    it 'renders show template' do
      nutrition_plan = create(:nutrition_plan)
      get :show, id: nutrition_plan
      expect(response).to render_template('show')
    end

    it 'assigns requested nutrition plan to @nutrition_plan' do
      nutrition_plan = create(:nutrition_plan)
      get :show, id: nutrition_plan
      expect(assigns(:nutrition_plan)).to eq(nutrition_plan)
    end
  end

  describe 'GET #new' do
    it 'renders new template' do
      get :new
      expect(response).to render_template('new')
    end

    it 'assigns @nutrition_plan' do
      get :new
      expect(assigns(:nutrition_plan)).not_to be_nil
    end
  end

  describe 'GET #edit' do
    it 'renders edit template' do
      nutrition_plan = create(:nutrition_plan)
      get :edit, id: nutrition_plan
      expect(response).to render_template('edit')
    end

    it 'assigns @nutrition_plan' do
      nutrition_plan = create(:nutrition_plan)
      get :edit, id: nutrition_plan
      expect(assigns(:nutrition_plan)).not_to be_nil
    end
  end

  describe 'GET #show_dishes' do
    before(:each) do
      @nutrition_plan = create(:nutrition_plan)
    end

    it 'renders show_dishes template' do
      get :show_dishes, id: @nutrition_plan
      expect(response).to render_template('show_dishes')
    end

    it 'assigns @dishes' do
      dish = create(:dish, name: 'Pizza')

      get :show_dishes, id: @nutrition_plan
      expect(assigns(:dishes)).to eq([dish])
    end
  end

  describe 'POST create' do

    context 'when valid attributes' do
      before :each do
        @nutrition_plan_attr = attributes_for(:nutrition_plan)
      end

      it 'creates a new nutrition plan' do
        expect do
          post :create, nutrition_plan: @nutrition_plan_attr
        end.to change(NutritionPlan, :count).by(1)
      end

      it 'redirects to the created nutrition plan' do
        post :create, nutrition_plan: @nutrition_plan_attr
        expect(response).to redirect_to(NutritionPlan.last)
      end
    end

    context 'when invalid attributes' do
      before :each do
        @nutrition_plan_attr =
          attributes_for(:invalid_nutrition_plan)
      end

      it 'does not create a new nutrition plan' do
        expect do
          post :create, nutrition_plan: @nutrition_plan_attr
        end.to_not change(NutritionPlan, :count)
      end

      it 'renders the same template' do
        post :create, nutrition_plan: @nutrition_plan_attr
        expect(response).to render_template('new')
      end
    end

  end

  describe 'POST add_dishes' do
    before :each do
      @nutrition_plan = create(:nutrition_plan)
      @nutrition_plan_attr = @nutrition_plan.attributes
      @dish = create(:dish)
      @nutrition_plan_attr[:dish_ids] = [@dish.id]
    end

    it 'adds dishes' do
      expect do
        post(
          :add_dishes,
          id: @nutrition_plan,
          nutrition_plan: @nutrition_plan_attr
        )
      end.to change(DishesNutritionPlan, :count).by(1)
    end

    it 'redirects to the nutrition plan' do
      post(
          :add_dishes,
          id: @nutrition_plan,
          nutrition_plan: @nutrition_plan_attr
        )
      expect(response).to redirect_to(@nutrition_plan)
    end
  end

  describe 'PUT update' do
    context 'when valid attributes' do
      before :each do
        @nutrition_plan = create(:nutrition_plan)
        @nutrition_plan_attr = attributes_for(
          :nutrition_plan,
          name: 'Plan 10'
        )
      end

      it 'updates existing nutrition plan' do
        put :update, id: @nutrition_plan, nutrition_plan: @nutrition_plan_attr
        @nutrition_plan.reload
        expect(@nutrition_plan.name).to eq('Plan 10')
      end

      it 'redirects to the updated nutrition plan' do
        post :update, id: @nutrition_plan, nutrition_plan: @nutrition_plan_attr
        expect(response).to redirect_to(@nutrition_plan)
      end
    end

    context 'when invalid attributes' do
      before :each do
        @nutrition_plan = create(:nutrition_plan)
        @nutrition_plan_attr = attributes_for(
          :nutrition_plan,
          name: ''
        )
      end

      it 'does not update existing nutrition plan' do
        put :update, id: @nutrition_plan, nutrition_plan: @nutrition_plan_attr
        @nutrition_plan.reload
        expect(@nutrition_plan.name).not_to eq('')
      end

      it 'renders the same template' do
        put :update, id: @nutrition_plan, nutrition_plan: @nutrition_plan_attr
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE destroy' do
    before :each do
      @nutrition_plan = create(:nutrition_plan)
    end

    it 'deletes the nutrition plan' do
      expect do
        delete :destroy, id: @nutrition_plan
      end.to change(NutritionPlan, :count).by(-1)
    end
  end

  describe 'DELETE remove_dish' do
    before :each do
      @nutrition_plan = create(:nutrition_plan, :with_dishes)
    end

    it 'removes the dish from the nutrition plan' do
      expect do
        delete(
          :remove_dish,
          id: @nutrition_plan,
          dish: @nutrition_plan.dishes.first
        )
      end.to change(DishesNutritionPlan, :count).by(-1)
    end
  end

end
