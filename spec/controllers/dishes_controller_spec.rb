require 'rails_helper'

describe DishesController, type: :controller do

  describe 'GET #index' do
    it 'renders index template' do
      get :index
      expect(response).to render_template('index')
    end

    it 'assigns @dishes' do
      dish = create(:dish)
      get :index
      expect(assigns(:dishes)).to eq([dish])
    end
  end

  describe 'GET #show' do
    it 'renders show template' do
      dish = create(:dish)
      get :show, id: dish
      expect(response).to render_template('show')
    end

    it 'assigns requested dish to @dish' do
      dish = create(:dish)
      get :show, id: dish
      expect(assigns(:dish)).to eq(dish)
    end
  end

  describe 'GET #new' do
    it 'renders new template' do
      get :new
      expect(response).to render_template('new')
    end

    it 'assigns @dish' do
      get :new
      expect(assigns(:dish)).not_to be_nil
    end
  end

  describe 'GET #edit' do
    it 'renders edit template' do
      dish = create(:dish)
      get :edit, id: dish
      expect(response).to render_template('edit')
    end

    it 'assigns @dish' do
      dish = create(:dish)
      get :edit, id: dish
      expect(assigns(:dish)).not_to be_nil
    end
  end

  describe 'POST create' do

    context 'when valid attributes' do
      before :each do
        @dish_attr = attributes_for(:dish)
      end

      it 'creates a new dish' do
        expect { post :create, dish: @dish_attr }.to change(Dish, :count).by(1)
      end

      it 'redirects to the created dish' do
        post :create, dish: @dish_attr
        expect(response).to redirect_to(Dish.last)
      end
    end

    context 'when invalid attributes' do
      before :each do
        @dish_attr = attributes_for(:invalid_dish)
      end

      it 'does not create a new dish' do
        expect { post :create, dish: @dish_attr }.to_not change(Dish, :count)
      end

      it 'renders the same template' do
        post :create, dish: @dish_attr
        expect(response).to render_template('new')
      end
    end
  end

  describe 'PUT update' do
    context 'when valid attributes' do
      before :each do
        @dish = create(:dish)
        @dish_attr = attributes_for(:dish, calories: 100)
      end

      it 'updates existing dish' do
        put :update, id: @dish, dish: @dish_attr
        @dish.reload
        expect(@dish.calories).to eq(100)
      end

      it 'redirects to the updated dish' do
        post :update, id: @dish, dish: @dish_attr
        expect(response).to redirect_to(@dish)
      end
    end

    context 'when invalid attributes' do
      before :each do
        @dish = create(:dish)
        @dish_attr = attributes_for(:dish, calories: -100)
      end

      it 'does not update existing dish' do
        put :update, id: @dish, dish: @dish_attr
        @dish.reload
        expect(@dish.calories).not_to eq(-100)
      end

      it 'renders the same template' do
        put :update, id: @dish, dish: @dish_attr
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE destroy' do
    before :each do
      @dish = create(:dish)
    end

    it 'deletes the dish' do
      expect { delete :destroy, id: @dish }.to change(Dish, :count).by(-1)
    end
  end
end
