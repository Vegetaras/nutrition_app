require 'rails_helper'

describe 'dishes/index.html.erb', type: :view do
  it 'displays all the dishes' do
    assign(:dishes, [
      create(:dish, name: 'Pizza'),
      create(:dish, name: 'Pasta')
    ])
    render

    expect(rendered).to match(/Pizza/)
    expect(rendered).to match(/Pasta/)
  end

  it 'handles empty list' do
    assign(:dishes, [])
    render

    expect(rendered).to match(/There are no dishes/)
  end
end
