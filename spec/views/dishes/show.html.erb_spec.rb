require 'rails_helper'

describe 'dishes/show.html.erb', type: :view do
  it 'displays the dish' do
    assign(:dish,  create(:dish, name: 'Pizza'))
    render
    expect(rendered).to match(/Pizza/)
  end
end
