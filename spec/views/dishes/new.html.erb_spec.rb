require 'rails_helper'

describe 'dishes/new.html.erb', type: :view do
  before(:each) do
    @dish = assign(:dish, create(:dish, name: 'Pizza'))
  end

  it 'renders the dish partial' do
    stub_template 'dishes/_dish.html.erb' => @dish.name
    render partial: 'dish'
    expect(rendered).to match(/Pizza/)
  end
end
