require 'rails_helper'

describe 'dishes/_dish.html.erb', type: :view do
  it 'displays the dish' do
    dish = assign(:dish, create(:dish, name: 'Pizza'))
    render dish
    expect(rendered).to match(/Pizza/)
  end
end
