require 'rails_helper'

describe 'nutrition_plans/new.html.erb', type: :view do
  before(:each) do
    @nutrition_plan = assign(
      :nutrition_plan,
      create(:nutrition_plan, name: 'Plan 1')
    )
  end

  it 'renders the nutrition plan partial' do
    stub_template(
      'nutrition_plans/_nutrition_plan.html.erb' => @nutrition_plan.name
    )
    render partial: 'nutrition_plan'
    expect(rendered).to match(/Plan 1/)
  end
end
