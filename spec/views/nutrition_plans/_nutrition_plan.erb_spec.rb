require 'rails_helper'

describe 'nutrition_plans/nutrition_plan.html.erb', type: :view do
  it 'displays the nutrition plan' do
    nutrition_plan = assign(
      :nutrition_plan,
      create(:nutrition_plan, name: 'Plan 1')
    )
    render nutrition_plan
    expect(rendered).to match(/Plan 1/)
  end
end
