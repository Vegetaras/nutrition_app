require 'rails_helper'

describe 'nutrition_plans/index.html.erb', type: :view do
  it 'displays all the nutrition_plans' do
    assign(:nutrition_plans, [
      create(:dish, name: 'Plan 1'),
      create(:dish, name: 'Plan 2')
    ])
    render

    expect(rendered).to match(/Plan 1/)
    expect(rendered).to match(/Plan 2/)
  end

  it 'handles empty list' do
    assign(:nutrition_plans, [])
    render

    expect(rendered).to match(/There are no nutrition plans/)
  end
end
