require 'rails_helper'

describe 'nutrition_plans/show.html.erb', type: :view do

  it 'displays the nutrition plan' do
    assign(:nutrition_plan, create(:nutrition_plan, name: 'Plan 1'))
    render
    expect(rendered).to match(/Plan 1/)
  end

  it 'displays nutrition plan\'s dishes' do
    assign(:nutrition_plan, create(:nutrition_plan, :with_dish))
    render
    expect(rendered).to match(/Plan 1/)
    expect(rendered).to match(/Dish 1/)
  end

  it 'displays total nutritional value' do
    assign(:nutrition_plan, create(:nutrition_plan, :with_dishes))
    render
    expect(rendered).to match(/Calories: 2/)
    expect(rendered).to match(/Protein: 2/)
    expect(rendered).to match(/Carbohydrates: 2/)
    expect(rendered).to match(/Fat: 2/)
  end
end
