FactoryGirl.define do
  factory :dish do
    sequence(:name) { |n| "Dish #{n}" }
    calories 1
    protein 1
    carbohydrates 1
    fat 1

    trait :with_nutrition_plans do
      nutrition_plans { create_list(:nutrition_plan, 1) }
    end
  end

  factory :invalid_dish, parent: :dish do
    calories(-1)
  end
end
