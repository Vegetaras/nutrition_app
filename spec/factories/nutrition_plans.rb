FactoryGirl.define do
  factory :nutrition_plan do
    sequence(:name) { |n| "Plan #{n}" }

    trait :with_dishes do
      dishes { create_list(:dish, 2) }
    end

    trait :with_dish do
      dishes { create_list(:dish, 1, name: 'Dish 1') }
    end
  end

  factory :invalid_nutrition_plan, parent: :nutrition_plan do
    name ''
  end

end
