require 'rails_helper'

describe NutritionPlan, type: :model do
  it { is_expected.to respond_to(:name) }

  it 'has unique name' do
    create(:nutrition_plan, name: 'Plan 1')
    nutrition_plan = build(:nutrition_plan, name: 'Plan 1')
    expect(nutrition_plan).not_to be_valid
  end

  it { is_expected.to have_many(:dishes) }

  it 'returns total calories' do
    nutrition_plan = create(:nutrition_plan, :with_dishes)
    total = nutrition_plan.total_nutritional_value

    expect(total[:calories]).to eq(2)
    expect(total[:protein]).to eq(2)
    expect(total[:carbohydrates]).to eq(2)
    expect(total[:fat]).to eq(2)
  end
end
