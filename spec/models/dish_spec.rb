require 'rails_helper'

describe Dish, type: :model do

  it 'has a valid factory' do
    factory = build(:dish)
    expect(factory).to be_valid
  end

  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:calories) }
  it { is_expected.to respond_to(:protein) }
  it { is_expected.to respond_to(:carbohydrates) }
  it { is_expected.to respond_to(:fat) }

  it 'has unique name' do
    create(:dish, name: 'Pizza')
    dish = build(:dish, name: 'Pizza')
    expect(dish).not_to be_valid
  end

  it 'has positive nutritional value' do
    dish = build(
      :dish,
      calories: 1,
      protein: 1,
      carbohydrates: 1,
      fat: 1
    )

    expect(dish).to be_valid
  end

  it 'cannot have negative nutritional value' do
    dish = build(
      :dish,
      calories: -1,
      protein: -1,
      carbohydrates: -1,
      fat: -1
    )

    expect(dish).not_to be_valid
  end

  it { is_expected.to have_many(:nutrition_plans) }
end
